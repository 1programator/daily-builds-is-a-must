<?php
require('../farm2/crondata/db.php');

$sql_aktuel = "UPDATE `oc_products`, `oc_furnizor_manual` SET `oc_products`.`bck_pret` = `oc_products`.`pret`, `oc_products`.`pret` = `oc_furnizor_manual`.`pret_net` WHERE
`oc_products`.`cod_CIM` = `oc_furnizor_manual`.`cod_cnas` AND `oc_products`.`tva` = `oc_furnizor_manual`.`cota_tva` AND `oc_products`.`bck_pret` = 0";
// cod pentru 1.. salvare preturi anterioare in coloana `oc_products`.`bck_pret` si 2.. urcare preturi in site `oc_products`.`pret_fara_tva`
if(mysqli_query($link, $sql_aktuel) === TRUE) {
  echo "Produsele din nomenclatorul furnizorului au fost actualizate in baza de date de pe server. Preturile au fost actualizate.";
  echo "Pentru a reveni la preturile anterioare apasa butonul de mai jos.";
} else { echo "A survenit o eroare, pretul produselor din nomenclatorul furnizorului NU a fost actualizat la produsele de pe site. Te rugam verifica algoritmul din codul sursa.";}

echo "<button>Restaureaza preturile anterioare pentru acest furnizor.</button>";
//

$sql_restorepr_furni = "UPDATE `oc_products`, `oc_furnizor_manual` SET `oc_products`.`pret` = `oc_products`.`bck_pret`, `oc_products`.`bck_pret` = '0' WHERE
`oc_products`.`bck_pret` != '0' AND `oc_products`.`cod_CIM` = `oc_furnizor_manual`.`cod_cnas`";
// cod pentru 1.. salvare preturi anterioare in coloana `oc_products`.`bck_pret` si 2.. urcare preturi in site `oc_products`.`pret_fara_tva`
if(mysqli_query($link, $sql_restorepr_furni) === TRUE) {
  echo "Preturile produselor din nomenclatorul furnizorului au fost restaurate la valorile initiale in baza de date de pe server.";
} else { echo "A survenit o eroare, restaurarea preturilor nu a avut loc. Te rugam verifica algoritmul din codul sursa.";}
 ?>
